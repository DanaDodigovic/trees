#include "AVLTree.hpp"

// TODO: remove me
#include <iostream>
#include <iomanip>
#include <math.h>

void AVLTree::insert(double element)
{
    if (!root) {
        root = std::make_unique<Node>(element);
    } else {
        insert_non_empty(root, element);
    }
}

void AVLTree::print()
{
    ElementsByLevel elements(get_height());
    set_inorder_elements(elements, *root, get_height());

    for (int i = elements.size() - 1; i >= 0; i--) {
        int n_of_spaces = std::pow(2, i + 1) / 2;
        for (unsigned j = 0; j < elements[i].size(); j++) {
            if (elements[i][j].has_value()) {
                std::cout << std::setw(n_of_spaces) << elements[i][j].value()
                          << std::setw(n_of_spaces) << " ";
            } else {
                std::cout << std::setw(n_of_spaces) <<  "x"
                          << std::setw(n_of_spaces) << " ";
            }
        }
        std::cout << std::endl;
    }
}

void AVLTree::insert_non_empty(std::unique_ptr<BinarySearchTree::Node>& node,
                               double element)
{
    if (element < node->get_element()) {
        if (!node->has_left_child()) {
            node->create_left_child(element);
        } else {
            insert_non_empty(node->left_child, element);
        }
    } else if (element > node->get_element()) {
        if (!node->has_right_child()) {
            node->create_right_child(element);
        } else {
            insert_non_empty(node->right_child, element);
        }
    } else {
        throw std::runtime_error("Element already exists!");
    }

    node->update_height();

    int balance_factor = get_balance_factor(*node);

    if (balance_factor > 1 && element < node->get_left_child().get_element()) {
        rotate_right(node);
    }
    if (balance_factor < -1 &&
        element > node->get_right_child().get_element()) {
        rotate_left(node);
    }
    if (balance_factor > 1 && element > node->get_left_child().get_element()) {
        rotate_left(node->left_child);
        rotate_right(node);
    }
    if (balance_factor < -1 &&
        element < node->get_right_child().get_element()) {
        rotate_right(node->right_child);
        rotate_left(node);
    }
}

void AVLTree::rotate_right(std::unique_ptr<BinarySearchTree::Node>& y)
{
    std::unique_ptr<BinarySearchTree::Node> x = std::move(y->left_child);

    y->left_child  = std::move(x->right_child);
    x->right_child = std::move(y);

    y = std::move(x);

    y->right_child->update_height();
    y->update_height();
}

void AVLTree::rotate_left(std::unique_ptr<BinarySearchTree::Node>& x)
{
    std::unique_ptr<BinarySearchTree::Node> y = std::move(x->right_child);

    x->right_child = std::move(y->left_child);
    y->left_child  = std::move(x);

    x = std::move(y);

    x->left_child->update_height();
    x->update_height();
}

int AVLTree::get_balance_factor(const BinarySearchTree::Node& node)
{
    int left_height =
        node.has_left_child() ? node.get_left_child().get_height() : 0;
    int right_height =
        node.has_right_child() ? node.get_right_child().get_height() : 0;
    return left_height - right_height;
}

void AVLTree::set_inorder_elements(ElementsByLevel& elements,
                                   const BinarySearchTree::Node& node,
                                   unsigned height)
{
    if (node.has_left_child()) {
        set_inorder_elements(elements, node.get_left_child(), height - 1);
    }

    elements[height - 1].push_back(node.get_element());

    if (!node.has_left_child() && height == 2) { elements[0].push_back({}); }
    if (!node.has_right_child() && height == 2) { elements[0].push_back({}); }
    if (!node.has_left_child() && height == 3) {
        elements[1].push_back({});
        elements[0].push_back({});
        elements[0].push_back({});
    }
    if (!node.has_right_child() && height == 3) {
        elements[1].push_back({});
        elements[0].push_back({});
        elements[0].push_back({});
    }

    if (node.has_right_child()) {
        set_inorder_elements(elements, node.get_right_child(), height - 1);
    }
}
