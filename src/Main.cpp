#include "AVLTree.hpp"

#include <vector>
#include <fstream>
#include <iostream>

int main(void)
{
    std::vector<double> elements;
    std::ifstream fis("res/tree1.txt");
    double element;
    while (fis >> element) elements.push_back(element);

    AVLTree tree;

    for (auto& element : elements) tree.insert(element);
    tree.print();


    while (true) {
        std::cout << "\nNovi element: ";
        std::cin >> element; 
        tree.insert(element);
        tree.print();
    }

    return 0;
}
