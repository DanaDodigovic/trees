#include <catch2/catch.hpp>

#include "BinarySearchTree.hpp"
#include "AVLTree.hpp"

#include <sstream>

TEST_CASE("BST Test")
{
    BinarySearchTree bst;
    bst.insert(5);
    bst.insert(7);
    bst.insert(2);
    bst.insert(1);
    bst.insert(8);
    bst.insert(9);
    bst.insert(4);

    SECTION("Inorder output")
    {
        std::stringstream ss;
        bst.inorder(ss);
        REQUIRE(ss.str() == "1 2 4 5 7 8 9 ");
    }

    SECTION("Preorder output")
    {
        std::stringstream ss;
        bst.preorder(ss);
        REQUIRE(ss.str() == "5 2 1 4 7 8 9 ");
    }

    SECTION("Duplicate insertion")
    {
        try {
            bst.insert(2);
        } catch (const std::runtime_error& exc) {
            std::stringstream ss;
            bst.inorder(ss);
            REQUIRE(ss.str() == "1 2 4 5 7 8 9 ");
        }
    }

    SECTION("Tree height") { REQUIRE(bst.get_height() == 4); }
}

TEST_CASE("Left left case")
{
    AVLTree avl;
    avl.insert(13);
    avl.insert(10);
    avl.insert(15);
    avl.insert(16);
    avl.insert(5);
    avl.insert(11);
    avl.insert(4);
    avl.insert(8);

    SECTION("Left left case")
    {
        avl.insert(3);
        {
            std::stringstream ss;
            avl.inorder(ss);
            REQUIRE(ss.str() == "3 4 5 8 10 11 13 15 16 ");
        }
        {
            std::stringstream ss;
            avl.preorder(ss);
            REQUIRE(ss.str() == "13 5 4 3 10 8 11 15 16 ");
        }
    }
}

TEST_CASE("Right right case")
{
    AVLTree avl;
    avl.insert(30);
    avl.insert(5);
    avl.insert(35);
    avl.insert(32);
    avl.insert(40);

    avl.insert(45);
    {
        std::stringstream ss;
        avl.inorder(ss);
        REQUIRE(ss.str() == "5 30 32 35 40 45 ");
    }
    {
        std::stringstream ss;
        avl.preorder(ss);
        REQUIRE(ss.str() == "35 30 5 32 40 45 ");
    }
}

TEST_CASE("Left right case")
{
    AVLTree avl;
    avl.insert(13);
    avl.insert(10);
    avl.insert(15);
    avl.insert(5);
    avl.insert(11);
    avl.insert(16);
    avl.insert(4);
    avl.insert(6);

    avl.insert(7);
    {
        std::stringstream ss;
        avl.inorder(ss);
        REQUIRE(ss.str() == "4 5 6 7 10 11 13 15 16 ");
    }

    {
        std::stringstream ss;
        avl.preorder(ss);
        REQUIRE(ss.str() == "13 6 5 4 10 7 11 15 16 ");
    }
}

TEST_CASE("Right left case")
{
    AVLTree avl;
    avl.insert(5);
    avl.insert(2);
    avl.insert(7);
    avl.insert(1);
    avl.insert(4);
    avl.insert(6);
    avl.insert(9);
    avl.insert(16);

    avl.insert(15);
    {
        std::stringstream ss;
        avl.inorder(ss);
        REQUIRE(ss.str() == "1 2 4 5 6 7 9 15 16 ");
    }

    {
        std::stringstream ss;
        avl.preorder(ss);
        REQUIRE(ss.str() == "5 2 1 4 7 6 15 9 16 ");
    }
}

TEST_CASE("Random cases") {
    AVLTree avl;
    avl.insert(2);
    avl.insert(4);
    avl.insert(7);
    avl.insert(5);
    avl.insert(6);
    avl.insert(10);
    avl.insert(20);
    avl.insert(8);
    avl.insert(65);
    avl.insert(120);
    avl.insert(1);
    avl.insert(0);
    avl.insert(3);
    avl.insert(1200);

    std::stringstream ss;
    avl.preorder(ss);
    REQUIRE(ss.str() == "6 2 1 0 4 3 5 10 7 8 65 20 120 1200 ");
}

TEST_CASE("Print tree") {
    AVLTree avl;
    avl.insert(5);
    avl.insert(2);
    avl.insert(7);
    avl.insert(1);
    avl.insert(4);
    avl.insert(6);
    avl.insert(0);

    avl.print();
}

TEST_CASE("Test tree") {
    AVLTree avl;
    avl.insert(18);
    avl.insert(31);
    avl.insert(33);
    avl.insert(17);
    avl.insert(23);
    avl.insert(37);
    avl.insert(3);
    avl.insert(8);
    avl.insert(1);
    avl.insert(40);

    avl.print();
}
